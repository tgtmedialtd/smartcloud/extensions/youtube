let core:any
try { core = require('@smartcloud/core').core } catch (_) {}
const log = (data: string | undefined, level: number | undefined) => {try {core.log(data,level,)} catch { console.log(data)}}

export class Init {
  constructor(){
  }

}

export class Shutdown {
  constructor(){
  }

}

export class Api {
  message:any
  api=require(`${__dirname}/routes/api`)
  constructor(message:any){
    this.message = message
    this.find()
  }
  find () {
    let actions = this.api.actions
    for(var action in actions) {
      if (action.toLowerCase() == this.message.action.toLowerCase()) actions[action].function(this.message)
    }
  }

}

export function smartcloud () {
    const data = {
      file: `${__dirname}/smartcloud.js`
    }
    return data;
}
